```mermaid
classDiagram

class VirtualResource

Resource --o Resource
Resource <|-- VirtualResource

VirtualResource <|-- DataResource
VirtualResource <|-- SoftwareResource
```