# Self-Description Landing Page

Welcome to the Gaia-X Self-Description landing page.

Content to be added:

* **Introduction** to Self-Descriptions and basic **Terminology**
* **Taxonomy** of Service Offering and Resource classes
* **Attributes** for describing any of these classes
* **Tutorials** on how to write Self-Descriptions for various use cases
* **Tools** for End Users
* **Technical implementation** of Taxonomy and Attributes as **Ontologies** and **Validation Shapes**, including **Documentation**
