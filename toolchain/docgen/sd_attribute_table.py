from datetime import datetime

import os
import typing
import yaml

def read_content_file(path: str) -> str:
    with open(os.path.abspath(path)) as file:
        return file.readlines()


def get_version() -> str:
    revision_state = datetime.now()
    return revision_state.strftime("%d/%m/%Y %H:%M:%S")


def generate_outline(names: typing.List[str]) -> str:
    """
    Returns outline in markdown for Service Taxonomy.
    :param names: name of all classes to be included in outline
    :return: markdown outline
    """
    output_str = "## Outline\n\n"
    for name in names:
        entry = get_class_name_from_file(name)
        output_str = output_str + "- [" + entry + "](#" + entry.lower() + ")\n"
    return output_str + "\n\n"


def get_class_name_from_file(file_name: str) -> str:
    """
    Converts filename to name of class in Conceptual Model given yaml file belongs to. Dashes are removed and each
    string after a dash is capitalized. E.g. bare_metal_node returns BareMetalNode.
    :param file_name: Name of file without extension
    :type file_name: str
    :return: class name
    :rtype: str
    """
    parts = file_name.split("-")
    return_string = ""
    for p in parts:
        return_string = return_string + p.capitalize()

    return return_string


def convert_list_to_string(list_of_strings: typing.List[str], add_link: bool = False) -> str:
    """
    Converts list taken from yaml files to a string to be inserted in markdown.
    :param list_of_strings: list of strings
    :type list_of_strings: typing.List[str]
    :param add_link: true if link in markdown file should be added
    :type add_link: bool
    :return: list as string
    :rtype: str
    """
    if list_of_strings is None:
        return ""
    string = ""
    for item in list_of_strings:
        if string != "":
            string = string + ", "

        if add_link:
            string = string + "[" + str(item) + "](#" + str(item).lower() + ")"
        else:
            string = string + str(item)

    return string


def generate_markdown_table(file: str) -> str:
    """
    Generates a markdown table of attributes specified in given yaml file.
    :param file: Path to yaml file
    :type file: str
    :return: table of attributes in markdown
    """

    with open(file, "r") as stream:
        try:
            attributes = yaml.safe_load(stream)
            class_name = next(iter(attributes))
            if 'subClassOf' in attributes[class_name].keys():
                super_class_list = attributes[class_name]['subClassOf']
            else:
                super_class_list = None

            markdown_table = " **Super class**: " + convert_list_to_string(super_class_list, True) + "\n\n" \
                                    "| Title | Data Type | Min Count | Max Count | Description | Example Value | \n" \
                                    "| ------- | ------- | ------- | ------- | ------- | ------- | \n " \
                                    ""
            for a in attributes[class_name]['attributes']:
                print("   ... parse attribute " + a['title'])
                markdown_table = markdown_table + "| " + a['title'] + " | " \
                                 + a['dataType'] + " | "
                if 'cardinality' in a.keys() and \
                        'minCount' in a['cardinality'].keys():
                    markdown_table = markdown_table + str(a['cardinality']['minCount']) + " | "
                else:
                    markdown_table = markdown_table + " 0 | "
                if 'cardinality' in a.keys() and \
                        'maxCount' in a['cardinality'].keys():
                    markdown_table = markdown_table + str(a['cardinality']['maxCount']) + " | "
                else:
                    markdown_table = markdown_table + " unlimited | "

                markdown_table = markdown_table + "" \
                                 + a['description'] + " | " \
                                 + convert_list_to_string(a['exampleValues']) + " | \n"
        except yaml.YAMLError as exc:
            print(exc)

    return markdown_table


def sort_files_by_name(dir: str, extensions: typing.List[str] = None) -> typing.List[str]:
    """
    Sorts list of files stored in given ``dir`` sorted by name in ascending order.
    :param dir: directory to look for files
    :type dir: str
    :param extensions: list of file extensions to be included. Files having an extension not being in list ``extension``
    will not be considered.
    :return sorted list of file names
    :rtype: typing.List<str>
    """
    files_names = []
    for file_name in os.listdir(os.path.abspath(dir)):
        name, file_extension = os.path.splitext(file_name)
        if extensions and file_extension in extensions:
            files_names.append(name)
        if not extensions:
            files_names.append(name)
    files_names.sort()
    return files_names


def remove_extensions_from_files(file_names: typing.List[str]) -> typing.List[str]:
    names = []
    for name in file_names:
        name_without_extension, _ = os.path.splitext(name)
        names.append(name_without_extension)
    return names


def write_markdown(src_yaml: str,
                   src_concept: str = None,
                   output_file: str = "sd-attributes.md") -> None:
    """
    Writes markdown file for service taxonomy. There is one page for each yaml file. Each Page includes conceptual model
    followed by table of attributes from yaml file.
    #:param src_yaml: Source folder for yaml files (single-point-of-truth for attributes)
    #:type str
    #:param src_concept: Source folder for conceptual model files. There has to be one file per class in Conceptual
                        Model and yaml file.
    #:type str
    #:param file_name: name of output file placed in current folder. Default name 'documentation.md'
   # :type str
    """

    # list of all yaml files in ascending order
    file_names = sort_files_by_name(src_yaml, extensions=[".yaml"])
    file_names = remove_extensions_from_files(file_names)

    # write title and outline
    output_file = open(os.path.abspath(output_file), "w")
    output_file.writelines("# Self Description Attributes - Overview\n")

    output_file.writelines("\n")
    output_file.writelines("Version: " + get_version() + "\n\n\n")
    output_file.writelines(generate_outline(file_names))

    # add conceptual models
    try:
        for name in file_names:
            # write section title
            class_name = get_class_name_from_file(name)
            output_file.writelines("# " + class_name + "\n")

            # write conceptual model
            # Todo: add automatic generation of mermaid diagrams
            #print("Read conceptual model file for '" + class_name + "'")
            #conceptual_model_file = os.path.join(src_concept, name + ".md")
            #conceptual_model = read_content_file(conceptual_model_file)
            #output_file.writelines(conceptual_model)
            #output_file.writelines("\n")

            # write attribute table
            print("Read yaml file for class '" + class_name + "'")
            yaml_file = os.path.join(src_yaml, name + ".yaml")
            attribute_table = generate_markdown_table(yaml_file)
            output_file.writelines(attribute_table)
            output_file.writelines("\n\n\n")
    finally:
        output_file.close()
        
